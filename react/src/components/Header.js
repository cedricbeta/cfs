import React from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
import { NavLink } from 'react-router-dom'
import "../styles/index.css";
import { useSelector } from "react-redux";

import Account from "./Account";

const Header = () => {
    const data = useSelector((state) => state.blockchain.value);
  const isConnected = data.account !== "";
    return (
        <>
            <Navbar bg="dark" variant="dark">
                <Container>
                <h1 className="title">CFS</h1>
                    <NavLink to="/" className="text-decoration-none text-light mx-2">User Registration</NavLink>
                    <Nav className="me-auto">
                        <NavLink to="/" className="text-decoration-none text-light mx-2">Home</NavLink>
                        
                    </Nav>
                    
                        <div className="note">
                            {isConnected ? (
                            <p>
                                Note: You are currently connected to the{" "}
                                {data.network ? data.network : "unknown"} network
                            </p>
                            ) : (
                            <p>Please connect your wallet</p>
                            )}
                        </div>
                        <Account />
                </Container>
            </Navbar>
        </>
    )
}

export default Header