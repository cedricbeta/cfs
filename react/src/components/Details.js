import React, { useEffect, useState } from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import { useNavigate } from 'react-router-dom'
import "../styles/index.css";
import { useSelector } from "react-redux";
import Account from "./Account";
import FileStorage from "./FileStorage";

const Details = () => {
    const data = useSelector((state) => state.blockchain.value);
    const isConnected = data.account !== "";


    const [logindata, setLoginData] = useState([]);


    const history = useNavigate();

    const [show, setShow] = useState(false);

    var todayDate = new Date().toISOString().slice(0, 10);
  

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const Birthday = () => {
        const getuser = localStorage.getItem("user_login");
        if (getuser && getuser.length) {
            const user = JSON.parse(getuser);
         
            setLoginData(user);


            const userbirth = logindata.map((el, k) => {
                return el.date === todayDate
            });

            if (userbirth) {
                setTimeout(() => {
                    console.log("ok");
                    handleShow();
                }, 3000)
            }
        }
    }

    const userlogout = ()=>{
        localStorage.removeItem("user_login")
        history("/");
    }

    useEffect(() => {
        Birthday();
    }, [])

    return (
        <>
            {
                logindata.length === 0 ? "errror" :
                    <>
                        <h1>Hello, {logindata[0].name}</h1>

                        <h2>You have been loggeg in CFS successfully!</h2>
                        <h3>Please check in your files / folder here: </h3>
                        <br />
                        <div>
                            {isConnected ? (
                            <>
                                <FileStorage />
                            </>
                            ) : <h2> Please connect your metamask account. </h2>}
                        </div>
                        <div align="center">
                        <Button onClick={userlogout} >LogOut</Button>
                        </div>
                        
                        
                     
                    </>
            }
        </>
    )
}

export default Details






















