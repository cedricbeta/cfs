import { Web3Storage } from "web3.storage";

const web3storage_key = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJkaWQ6ZXRocjoweGM1N0I1MDFGYzkxODFFYUVhNmRBMmQ3OTgyRTFCM0Q5ZTJBMjg0MUYiLCJpc3MiOiJ3ZWIzLXN0b3JhZ2UiLCJpYXQiOjE2ODU2MTUyNDUxNzEsIm5hbWUiOiJicm93bmllIn0.IhkOCY8zAAJLUlKqoStrmxZ2Jak_rTHXoJNYxWORrI8";

export const IPFS_GATEWAY = "https://ipfs.io/ipfs/";

function GetAccessToken() {
  return web3storage_key;
}

function MakeStorageClient() {
  return new Web3Storage({ token: GetAccessToken() });
}

export const StoreContent = async (content) => {
  console.log("Uploading content to IPFS with web3.storage....");
  const client = MakeStorageClient();
  const cid = await client.put([content]);
  console.log("Stored content with cid:", cid);
  return cid;
};

export const StoreManyFiles = async (files) => {
  console.log("Uploading files to IPFS with web3.storage....");
  const client = MakeStorageClient();
  const cid = await client.put(files);
  console.log("Stored files with cid:", cid);
  return cid;
};
