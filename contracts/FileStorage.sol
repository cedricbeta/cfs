pragma solidity ^0.8.0;


contract FileStorage {
    address payable public owner;
    uint256 public uploadFee;
    address public priceFeedAddress;

    mapping(address => File[]) public usersFiles;

    event FileAdded(address indexed user, string fileName);

    struct File {
        string name;
        uint256 size;
        string uri;
        uint256 ts;
    }

    constructor(address _priceFeedAddress) {
        priceFeedAddress = _priceFeedAddress;
        owner = payable(msg.sender);
    }


    function uploadFile(string memory _name, uint256 file_size, string memory f_URI) public payable 
    {
        File memory newFile = File(_name, file_size, f_URI, block.timestamp);
        usersFiles[msg.sender].push(newFile);

        emit FileAdded(msg.sender, _name);
        owner.transfer(msg.value);
    }

    function getUserFiles(address _user) public view returns (File[] memory) {
        require(msg.sender == _user, "You have onwership to the file!");
        return usersFiles[_user];
    }
}
