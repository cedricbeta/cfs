# CFS 

Chendong Wang

## Quick test please run 

```bash
ganache-cli -p 7545
```
In another terminal, run

```bash
chmod +x run.sh
./run.sh
```



One can modify the ipfs storage api key at `react/src/utils/StoreContent.js`



In this implementation, I make use of some of the existing front-end projects and the IPFS api to create an Cloud File Safe system, which can be refered to as an decentralized file sharing and access tool for everyone. In the implementation, we have two key design that maintain the integrity of CFS:

* We employ the username-password matching for first level login access to the functionality of the CFS.
* Then we apply the smart-contract-based address matching for further IPFS api access for file uploading and retrieval.

This provide us with higer safety degree by introducing a *2 factor authentication* approach.

I use brownie for contract connection and deployment and use react as my main front-end framework.

Here is the how the app works:

* Register

![fig_1](./figs/fig_1.png)

* Login

![fig_2](./figs/fig_2.png)

* Connect account (local ganache)

![fig_3](./fig_3.png)

![fig_4](/Users/chendong/Downloads/test-react/cfs/figs/fig_4.png)

* Choose the file

![fig_5](./figs/fig_5.png)

* Confirm the tx

![fig_6](./figs/fig_6.png)

* Results

![fig_7](./figs/fig_7.png)

We can also view at the web3.storage account we connected to.

![fig_8](./figs/fig_8.png)



